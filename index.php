<?php

require_once realpath("vendor/autoload.php");
require_once realpath("routes/api.php");
require_once realpath("routes/views.php");

use App\Core\Router;
use App\Core\Resolver;

$router = new Router();
    $router->addRoute($apiRoute);
    $router->addRouteGroup($viewsRoute);

$resolver = new Resolver($router);

$resolver->resolve();

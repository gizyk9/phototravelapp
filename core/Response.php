<?php

namespace App\Core;

class Response {
    public function asJson(array $response)
    {
        header('Content-type: application/json');
        echo json_encode($response);
    }

    public function asText(string $response)
    {
        header('Content-type: text/plain');
        echo $response;
    }

    public function asView(string $fileName)
    {
        include(__DIR__ ."/../views/$fileName.php");
    }
}
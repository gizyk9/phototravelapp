<?php

namespace App\Core;

use Exception;

class Route {
    private $children = [];
    private $name;
    private $controllerName;

    public function __construct(string $name, string $controllerName = '')
    {
        $this->name = $name;
        $this->controllerName = $controllerName;
    }

    public function addRoute(Route $route) {
        if ($this->hasRoute($route)) {
            throw new Exception("Route '$route->name' has already exist");            
        }

        array_push($this->children, $route);

        return $route;
    }

    private function hasRoute(Route $route) {
        foreach ($this->children as $child) {
            if ($child->name == $route->name) {
                return true;
            }
        }

        return false;
    }

    public function getController() {
        if ($this->controllerName == '') {
            throw new Exception('Controller not found');
        }
    
        return $this->controllerName;
    }

    public function findRoute($path, $i = 1)
    {
        if ($i == count($path)) {
            return $this;
        }

        foreach ($this->children as $childRoute) {
            if ($path[$i] == $childRoute->name) {
                return $childRoute->findRoute($path, $i + 1);
            }
        }

        throw new Exception('Route not found');
    }
}

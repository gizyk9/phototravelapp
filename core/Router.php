<?php

namespace App\Core;

class Router {
    private $globalRoute;

    public function __construct()
    {
        $this->globalRoute = new Route('');
    }

    public function addRoute(Route $route)
    {
        $this->globalRoute->addRoute($route);
    }

    public function addRouteGroup(RouteGroup $routeGroup)
    {
        $routes = $routeGroup->getRoutes();

        foreach ($routes as $route) {
            $this->globalRoute->addRoute($route);
        }
    }

    public function findRoute(): Route
    {
        $request_uri = explode('/', $_SERVER['REQUEST_URI']);

        return $this->globalRoute->findRoute($request_uri);
    }
}

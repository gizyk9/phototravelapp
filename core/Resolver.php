<?php

namespace App\Core;

use Exception;

class Resolver {
    private $router;
    private $controller;
    private $controllerClass;
    private $controllerMethod;

    public function __construct(Router $router)
    {
        $this->router = $router;
    }

    private function fetchController() {
        $controller = $this->router->findRoute()->getController();

        [$this->controllerClass, $this->controllerMethod] = explode('->', $controller);
    }

    private function resolveController()
    {
        if (!class_exists($this->controllerClass)) {
            throw new Exception("Controller '$this->controllerClass' does not exist");
        }

        $controllerClass = $this->controllerClass;

        $this->controller = new $controllerClass();
    }

    private function execMethod()
    {
        if(!method_exists($this->controller, $this->controllerMethod)) {
            throw new Exception("Method '$this->controllerMethod' is not defined in '$this->controllerClass' Controller");
        }
        
        $controller = $this->controller;
        $method = $this->controllerMethod;
        
        $controller->$method(new Request(), new Response());
    }

    public function resolve()
    {
        $this->fetchController();

        $this->resolveController();

        $this->execMethod();
    }
}

<?php

namespace App\Core;

use Exception;

class RouteGroup {
    private $children = [];

    public function addRoute(Route $route) {
        if ($this->hasRoute($route)) {
            throw new Exception("Route '$route->name' has already exist");            
        }

        array_push($this->children, $route);

        return $route;
    }

    private function hasRoute(Route $route) {
        foreach ($this->children as $child) {
            if ($child->name == $route->name) {
                return true;
            }
        }

        return false;
    }

    public function getRoutes()
    {
        return $this->children;
    }
}

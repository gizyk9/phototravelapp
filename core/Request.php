<?php

namespace App\Core;

class Request {
    private $body;

    public function __construct()
    {
        $this->body = json_decode(file_get_contents('php://input'));
    }

    public function getBody()
    {
        return $this->body;
    }
}
<?php

use App\Core\Route;

$apiRoute = new Route('api');

    $colors = new Route('colors');
    $apiRoute->addRoute($colors);

        $red = new Route('red', 'App\Controllers\ColorsController->red');
        $colors->addRoute($red);

        $blue = new Route('blue', 'App\Controllers\ColorsController->blue');
        $colors->addRoute($blue);

<?php

namespace App\Controllers;

use App\Core\Request;
use App\Core\Response;

class ColorsController {
    public function red(Request $req, Response $res) {
        $res->asJson([
            "color" => "red",
            "req" => $req->getBody(),
        ]);
    }

    public function home(Request $req, Response $res) {
        $res->asView("home");
    }
}
